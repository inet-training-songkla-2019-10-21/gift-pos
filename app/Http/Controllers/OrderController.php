<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Food;
use App\TableStore;
use App\Order_detail;

class OrderController extends Controller
{
    public function addTable($table_id)
    {
        $order = new Order();
        $order->table_id = $table_id;
        
        if($order->save()){
            return redirect('/order/'.$table_id.'/'.strval($order->id));
        }
        return redirect()->route('order.addorder');
    
    }

    public function addOrder($table_id, $order_id)
    {
        $foods = Food::all();
        $table = TableStore::find($table_id);
        $table->status = 2;
        // $orderdetail = Order_detail::with('food')->where('order_id', $order_id)->get();
        $orderdetail = Order_detail::where('order_id', $order_id)->get();

        if ($table->save()){
            return view('food.index', [
                'foods' => $foods,
                'table_id' => $table_id,
                'order_id' => $order_id,
                'orderdetail' => $orderdetail,
            ]);
        }
        return redirect()->route('table.index');
        // return view('food.index');
    }

    public function addFoodOrder($table_id, $order_id, Request $request)
    {   

        $orderdetail = new Order_detail();
        $orderdetail->order_id = $order_id ;
        $orderdetail->food_id = $request->id ;
        $orderdetail->food_name = $request->name;
        $orderdetail->amount = $request->amount ;
        $orderdetail->price = $request->price ;
        $orderdetail->total = $request->price * $request->amount;


        if($orderdetail->amount == 0){
            return redirect()->back();
        }
        if($orderdetail->save()){
            return redirect()->back();
        }
        return redirect()->back();
    }
}
