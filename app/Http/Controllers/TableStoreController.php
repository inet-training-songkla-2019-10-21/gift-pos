<?php

namespace App\Http\Controllers;

use App\TableStore;
use App\User;
use Illuminate\Http\Request;

class TableStoreController extends Controller
{
    public function index()
    {
        $tables = TableStore::all();
        return view('table.index', compact('tables'));
    }

    public function indexAdmin()
    {
        $tables = TableStore::all();
        return view('table.admin.index', compact('tables'));
    }


    public function createPage()
    {
        return view('table.admin.create');
    }

    public function create(Request $request)
    {
        //$user = new User();
        $table = new TableStore();

        $table->number = $request->number;
        $table->status = $request->status;
        //$table->update_by = $user->id;
        if(!$table->save()){
            return redirect()->route('admin.table.create.page');
        }
        return redirect()->route('admin.table.index');
    }

    public function editPage($id){

        $table = TableStore::find($id);
        return view('table.admin.edit', ['table' => $table]);
    }

    public function edit(Request $request)
    {
        $user = new User();
        $table = TableStore::find($request->id);
        $table->number = $request->number;
        $table->status = $request->status;
        //$table->update_by = $user->id;
        if(!$table->save()){
            return redirect()->route('admin.table.edit.page');
        }
        return redirect()->route('admin.table.index');
    }

    public function delete(TableStore $id){

        $id->delete();
        return redirect()->route('admin.table.index');
    }
}
