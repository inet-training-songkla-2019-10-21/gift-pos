<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Food;
use App\User;

class FoodController extends Controller
{
    public function info()
    {
        $foods = Food::all();
        return view('food.index', compact('foods'));
    }

    public function loginPage()
    {
        return view('food.admin.login');
    }

    public function login(Request $request)
    {
        $isAuth = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);
        if(!$isAuth) {
            return redirect()->back();
        }
        return redirect()->route('admin.table.index');
    }

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6|max:255|confirmed',
        ]);
        if(User::where('email', $request->email)->exists()) {
            return redirect()->back();
        }
        $validatedData['password'] = Hash::make($validatedData['password']);
       
        User::create($validatedData);

        $isAuth = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);
        if(!$isAuth) {
            return redirect()->back();
        }
        return redirect()->route('admin.table.index');
    }

    public function admin()
    {
        return view('admin');
    }

    public function infoAdmin()
    {
        $foods = Food::all();
        return view('food.admin.index', compact('foods'));
    }

    public function createPage()
    {
        return view('food.admin.create');
    }

    public function create(Request $request)
    {
        $food = new Food();
        $food->name = $request->name;
        $food->price = $request->price;
        $food->status = $request->status;

        if (!$food->save()) {
            return redirect()->route('admin.food.create.page');
        }
        return redirect()->route('admin.food.index');
    }

    public function editPage($id)
    {
        $food = Food::find($id);
        return view('food.admin.edit', ['food' => $food]);
    }

    public function edit(Request $request)
    {
        $food = Food::find($request->id);
        $food->name = $request->name;
        $food->price = $request->price;
        $food->status = $request->status;

        if (!$food->save()) {
            return redirect()->route('admin.food.edit.page');
        }
        return redirect()->route('admin.food.index');
    }

    public function delete(Food $id)
    {
//        if (empty(Auth::user())){
//            return redirect()->route('login');
//        }
        $id->delete();
        return redirect()->route('admin.food.index');
    }
}
