<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class OneChatController extends Controller
{
    public function getInfo()
    {
        $data = [
            'headers'=>[
                'Authorization'=>'Bearer A89b7f8c3cbc455db9cca395053740769b4f9a20dadd4424496a4e8374995d8d5aa8388e81d9a4c39b8e6c0c5763fa72c',
                'Content-Type'=>'application/json',
            ],
            'json'=>[
                'bot_id'=>"B9913ac8c5e1d55458a00489b48e8b8ce",
            ]
         ];
 
         try {
             $client = new Client([
                 'base_uri'=>'https://chat-manage.one.th:8997/api/v1/',
                 'verify'=> false,
             ]);
 
             $res = $client->request('POST','getlistroom',$data);
         }
         catch(ConnectException $e) {
             return $e->getMessage();
         }
         return json_encode(json_decode($res->getBody()->getContents()));
    }

    public function send($msg)
    {
        $data = [
            'headers'=>[
                'Authorization'=>'Bearer A89b7f8c3cbc455db9cca395053740769b4f9a20dadd4424496a4e8374995d8d5aa8388e81d9a4c39b8e6c0c5763fa72c',
                'Content-Type'=>'application/json',
            ],
            'json'=>[
                'to'=> "Ucac66a865f6656d9b451509bf3d721f4",
                'bot_id'=>"B9913ac8c5e1d55458a00489b48e8b8ce",
                "type" => "text",
	            "message" => $msg,
            ]
         ];
 
         try {
             $client = new Client([
                 'base_uri'=>'https://chat-public.one.th:8034/api/v1/',
                 'verify'=> false,
             ]);
 
             $res = $client->request('POST','push_message',$data);
         }
         catch(ConnectException $e) {
             return $e->getMessage();
         }
         return redirect()->route('table.index');
    }

}
