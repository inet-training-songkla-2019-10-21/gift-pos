<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\TableStore;

class BillController extends Controller
{
    public function billCheckPage()
    {

        $order = Order::with('table_stores', 'order_details')->where('status', '<' , 4)->get();
        // dd($order[1]->order_details[0]->food_name);
        return view('food.order', compact('order'));
    }

    public function billCheck($order_id)
    {

        $order = Order::find($order_id);
        $table = TableStore::find($order->id);
        
        

        if($order->status == 1){
            $order->status = 2;
        }elseif($order->status == 2){
            $order->status = 3;
        }elseif($order->status == 3){
            $table->status = 1;
            $order->status = 4;
        }else{
            return redirect()->back();
        }

        if($order->save()){
            if($table->save()){
                return redirect()->back();
            }
            return redirect()->back();
        }
        return redirect()->back();

    }
}
