<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableStore extends Model
{
    protected $fillable = [
        'number',
        'status',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class, 'table_id');
    }
}
