<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    protected $fillable = [
        'order_id',
        'food_id',
        'amount',
        'price',
        'total',
    ];

    public function foods()
    {
        return $this->belongTo(Food::class, 'id');
    }

    public function orders()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }
}
