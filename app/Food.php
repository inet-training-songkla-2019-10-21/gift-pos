<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable = [
        'name',
        'price',
        'status',
    ];

    public function order_details()
    {
        return $this->hasMany(Order_detail::class, 'food_id' , 'id');
    }
}
