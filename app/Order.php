<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Order extends Model
{
    protected $fillable = [
        'table_id',
        'status',
        'update_by',
    ];

    public function table_stores()
    {
        return $this->belongsTo(TableStore::class, 'table_id', 'id');
    }

    public function order_details()
    {
        return $this->hasMany(Order_detail::class, 'order_id');
    }
}
