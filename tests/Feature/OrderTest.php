<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Order;
use App\Order_detail;
use App\TableStore;

class OrderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testUseCanAddOrder()
    {
        $table_id = 1;
        $order = factory(Order::class)->create([
            'table_id' =>$table_id
        ]);
        
        $this->post(route('order.addorder'),[
            'table_id' => $table_id,
        ])->assertRedirect(route('order.addorder'));

        $this->assertDatabaseHas('orders',[
            'table_id' => $table_id,
        ]);

    }
}
