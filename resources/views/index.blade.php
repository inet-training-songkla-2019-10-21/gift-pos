<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Book Store</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css">--}}
{{--    <script src="{{asset('js/app.js')}}" defer></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style>
        body {
            background: #032c41;
            color: #ffffff;
            margin-top: 40px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }

    </style>
</head>
<body>
<br><br>
<div class="container">
    <div class="row">
        <div class="col-3">
            <h1><a href="{{route('book.index')}}" class="main"><strong>Book Store</strong></a></h1>
        </div>
        <div class="col-2 offset-7" align="right">
            <h1><a href="{{route('book.create.page')}}" class="btn btn-primary">เพิ่มหนังสือ</a></h1>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-12">
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>

                    <th scope="col">หนังสือ</th>
                    <th scope="col">ผู้เขียน</th>
                    <th scope="col">คำบรรยาย</th>
                    <th scope="col">ราคา</th>
                    <th scope="col">ประเภท</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($books as $key => $book)
                    <tr>
                        <th scope="row">{{$key + 1}}</th>
                        <td>{{$book->name}}</td>
                        <td>{{$book->author}}</td>
                        <td>{{$book->describe}}</td>
                        <td>{{$book->price}}</td>
                        <td>{{$book->type}}</td>
                        <td><a href="{{route('book.edit.page', $book->id)}}" class="btn btn-outline-warning">แก้ไข</a></td>
                        <td><a href="{{route('book.delete', $book->id)}}" class="btn badge-danger">ลบ</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>