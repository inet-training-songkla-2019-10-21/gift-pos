<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Table store</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Add icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body {
            background: #032c41;
            background-image: url('https://www.hackensackmeridianhealth.org/wp-content/uploads/2018/10/healthy-foods.jpg');
            background-attachment: fixed; 
            color: #ffffff;
            margin-top: 20px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }

        /* button staff */
        .btnStaff {
            background-color: #e03914;
            border: none;
            color: white;
            padding: 12px 16px;
            font-size: 16px;
            cursor: pointer;
        }
        /* Darker background on mouse-over */
        .btnStaff:hover {
            background-color: #e86b2c;
            border-radius: 16px;
        
        }
        .grad {
            background: #FFFFFF; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* For Firefox 3.6 to 15 */
            background: linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* Standard syntax */
            padding-bottom: 20px;
        }

        .shadow:after{
          -moz-box-shadow: 5px 5px 5px #ccc;
            -webkit-box-shadow: 5px 5px 5px #ccc;
            box-shadow: 5px 5px 5px #ccc;

        }
        .shadow:hover {
            opacity: 1;
            transform: scale(1.10);
            }
    </style>
</head>
<body>
<div class="container " style=" background-color: #fff;">
    

            <div class="row">
                    <div class="col-5">
                        <div style=" padding-left: 15px; float: left;">
                            <img src="https://image.flaticon.com/icons/svg/415/415587.svg" width="100px"> 
                        </div>
                        <div style="margin-top:30px;">
                            <span style=" padding-left: 15px; margin-top:50px; font-size:3em; color:#A52A2A; font-weight:bold; ">   Gift POS</span>
                        </div>
                        </div>
                    <div class="col">
                     
                    </div>
                    <div class="col">

                        <div style="margin-top:30px;">
                            {{-- <ul class="nav nav-pills">
                                    <li class="nav-item">
                                      <a class="nav-link active" href="{{route('table.index')}}">GUEST</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" href="{{route('admin.table.index')}}">STAFF</a>
                                    </li>
                                  </ul> --}}
                            <ul class="nav justify-content-end">
                                    <li class="nav-item">
                                <a class="nav-link active bg-dark text-light" href="{{route('table.index')}}">GUEST</a>
                                    </li>
                                    <li class="nav-item">
                                <a class="nav-link" href="{{route('admin.table.index')}}">STAFF</a>
                                    </li>
                                </ul>
                        </div>
                    </div>
                  </div>
           

                    {{-- <div class="col">
                        
                    </div>
                    <div class="col">

                            <div class="text-dark"><br>
                                <img src="https://image.flaticon.com/icons/svg/415/415587.svg" width="40%"> 
                                 <h1 style="font-size:3em; font-weight:bold; ">Gift POS</h1></div>
                        
                    </div>
                    <div class="col">
                        
                    </div> --}}

                                {{-- <div  class="col-4 offset-3"><br>
                                    <ul class="nav justify-content-end">
                                        <li class="nav-item">
                                    <a class="nav-link active bg-dark text-light" href="{{route('table.index')}}">GUEST</a>
                                        </li>
                                        <li class="nav-item">
                                    <a class="nav-link" href="{{route('admin.table.index')}}">STAFF</a>
                                        </li>
                                    </ul>
                                </div> --}}
                        
    
</div>
<div class="container" style=" background-color: #fff; padding: 40px;">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" >
                <div class="carousel-inner">
                       <div class="carousel-item active">
                    <img style="height:8cm;" src="https://img.wongnai.com/p/1920x0/2017/03/15/a135ecdc17a3421a8e9f74bb6adf6808.jpg" class="d-block w-100">
                  </div> 
                        <div class="carousel-item">
                     <img style="height:8cm;" src="http://tv.bectero.com/wp-content/uploads/2018/04/DD-PAI-Group-Shot-8388.jpg" class="d-block w-100">
                  </div>   
                        <div class="carousel-item">
                     <img style="height:8cm;" src="https://food.mthai.com/app/uploads/2019/02/rabieng-lom-changwattana-1.jpg" class="d-block w-100">
                  </div>  
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
        </div>
</div>
              
    <div class="container" style=" background-color: #fff;" align ="center"> 
        <div class="row" >
    @foreach($tables as $key => $table)
    <div class="col-4"> 
    @if (($table->status)%2==1)
       <div class="card bg-dark text-light shadow" style="width: 8cm;"> 
            <img style="height:4cm;" src="https://ed.files-media.com/ud/book/content/1/154/459335/Kiew_Kai_Ka.jpg" class="card-img-top" alt=i>
    @else
        <div class="card bg-dark text-white" style="width: 8cm;"> 
        <img style="height:4cm;" src="https://ed.files-media.com/ud/book/content/1/154/459335/Kiew_Kai_Ka.jpg" class="card-img-top" alt=i>
        <img style="position:absolute;" class="card-img-top" src="https://pngimage.net/wp-content/uploads/2018/06/reserved-png-2.png" class="card-img-top" alt=i>
        
    @endif   
            <div class="card-body">
            <h5 class="card-title">Table : {{$table->number}}</h5>
            @if (($table->status)%2==1)
            <a class="btn btn-info" href="/order/{{$table->id}}">
                    <i class="fas fa-apple-alt"></i>ADD MENU</a>
             @else
             <p class="card-text">โต๊ะถูกจองไปแล้ว ไม่สามารถเลือกได้</p>
         @endif  
            </div>
    </div><br><br></div>
    @endforeach
    </div></div>
    <br><br>
</div>
</body>
</html>
