<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gift POS</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}">--}}
{{--    <script src="{{asset('js/app.js')}}"></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <style>
        body {
            background: #032c41;
            color: #ffffff;
            margin-top: 40px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }

    </style>
</head>
<body>
<br><br>
<div class="container">
    <div class="row">
        <div class="col-3">
            <h1><a href="{{route('admin.table.index')}}" class="main"><strong>Edit Table</strong></a></h1>
        </div>
    </div>
    <br><br>
    <div class="row justify-content-center">
        <div class="col-6" align="left" style="margin: 30px">
            <h2 style="color: #f3f3f3;"><u>แบบฟอร์มแก้ไขโต๊ะอาหาร</u></h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form action="{{route("admin.table.edit")}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="bookname">แก้ไขโต๊ะ</label>
                    <input
                        type="text" class="form-control"
                        name="number" id="tablename"
                        aria-describedby="tableHelp"
                        placeholder="ชื่อ โต๊ะ"
                        required
                        value={{$table->number}}
                    >
                </div>
                <div class="form-group">
                    <select multiple class="form-control" id="status" name="status">
                        @if ($table->status == 1)
                            <option value=1 selected="selected">ว่าง</option>
                            <option value=2>ไม่ว่าง</option>
                        @elseif ($table->status == 2)
                            <option value=1>ว่าง</option>
                            <option value=2 selected="selected">ไม่ว่าง</option>
                        @endif
                </select>
                     
                        
                    
                </div>
                <div class="form-group">
                    <input type="hidden" name="id" value="{{$table->id}}">
                </div>
                <br>
                <div align="right">
                    <button type="submit" class="btn btn-success">บันทึกการเปลี่ยนแปลง</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>