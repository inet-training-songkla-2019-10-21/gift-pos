<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gift POS</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css">--}}
{{--    <script src="{{asset('js/app.js')}}" defer></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style>
        body {
            background: #FFFFFF;
            background-image: url('https://www.hackensackmeridianhealth.org/wp-content/uploads/2018/10/healthy-foods.jpg');
            background-attachment: fixed;   
            color: #000000;
            margin-top: 20px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }

        .grad {
            background: #FFFFFF; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* For Firefox 3.6 to 15 */
            background: linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* Standard syntax */
            padding-bottom: 20px;
        }

        .shadow:after{
          -moz-box-shadow: 5px 5px 5px #ccc;
            -webkit-box-shadow: 5px 5px 5px #ccc;
            box-shadow: 5px 5px 5px #ccc;

        }
        .shadow:hover {
            opacity: 1;
            transform: scale(1.10);
            }

    </style>
</head>
<body>

    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">รายการอาหารที่เลือกไว้</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                 <table class="table">
                            <tr>
                                
                                        <th scope="col">ชื่ออาหาร</th>
                                        <th scope="col">ราคา</th>
                                        <th scope="col">จำนวน</th>
                                </tr>
                                <tbody>
                    @php
                        $sum = 0 ;
                        $count = 0 ;
                    @endphp
                    @foreach($orderdetail as $key => $orderfor)

          
                                            <tr>
                                          
                                              <td>{{$orderfor->food_name}}</td>
                                              <td>{{$orderfor->price}}</td>
                                              <td>{{$orderfor->amount}}</td>
                                            </tr>    
                        
                                            @php
                                              $sum += $orderfor->price * $orderfor->amount ;
                                              $count += 1 ;
                                            @endphp
                        
                    @endforeach

                    <tr style="background:#EEE;">
                        <th scope="row" colspan="2">รวมราคา</th>
                        <td>{{$sum}} บาท</td>
                      </tr>
                </tbody>
                </table>
        
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
              <a href="/onechat/message/อาหารถูกสั่ง"><button type="button" class="btn btn-primary" >เสร็จสิ้น</button></a>
            </div>
          </div>
        </div>
      </div>

<br><br>
<div class="container">
    <div class="row grad">
        <div class="col-2">
            <img src="https://image.flaticon.com/icons/svg/415/415587.svg" width="100%"> 
        </div>
        <div class="col-5">
            <span style="font-size:4em; font-weight:bold; "> Gift POS</span>
        <h1><a href="" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><ion-icon name="cart"></ion-icon>ตะกร้า Order <span class="badge badge-light">{{$count}}</span></a></h1>
            {{-- <h1><a href="{{route('book.index')}}" class="main"><strong>Book Store</strong></a></h1> --}}
        </div>
        <div class="col-3 offset-6" align="right">
                
            {{-- <h1><a href="{{route('book.create.page')}}" class="btn btn-primary">เพิ่มหนังสือ</a></h1> --}}
        </div>
    </div>

    <div style="  background: #FFFFFF; padding:20px" class="row">
    @foreach($foods as $key => $food)
    <form action="/order/{{$table_id}}/{{$order_id}}" method="post">
        @csrf
        @if($food->status==1)
        <div class="col">
            
            <div class="card shadow" style=" width: 18rem; margin: 20px;">
                    <img class="card-img-top" src="https://hhp-blog.s3.amazonaws.com/2019/07/burger.jpg" alt="Card image cap">
                    <div class="card-body">
                    <center><h5 style="color:#006600;" class="card-title" >{{$food->name}}</h5></center>
                    <br>
                    <input type="hidden" name="name" value="{{$food->name}}">
                    <input type="hidden" name="id" value="{{$food->id}}">
                    <input type="hidden" name="price" value="{{$food->price}}">
                      <div class="row">
                      <div class="col-7" >ราคา {{$food->price}} ฿</div>
                      <div class="col-5" align="right"> <input class="form-control" type="number" value="0" name="amount" id="amount"> </div>
                      </div>
                      <br>
                      <center><button type="submit" class="btn btn-success">เลือกเลย</button></center>
                      
                    </div>
                  </div>
        </div>



       
        @endif
    </form>


    @endforeach

    @foreach($foods as $key => $food)
    @if($food->status==2)
    <div class="col">
        
        <div class="card" style=" width: 18rem; margin: 20px;">
                <img class="card-img-top" src="https://hhp-blog.s3.amazonaws.com/2019/07/burger.jpg" alt="Card image cap">
                <img style="position:absolute;" class="card-img-top" src="https://149363977.v2.pressablecdn.com/wp-content/uploads/2019/03/sold-out-png-4.png" alt="Card image cap">
                <div class="card-body">
                <center><h5 style="color:#006600;" class="card-title" >{{$food->name}}</h5></center>
                <br>
                <input type="hidden" name="name" value="{{$food->name}}">
                <input type="hidden" name="id" value="{{$food->id}}">
                <input type="hidden" name="price" value="{{$food->price}}">
                  
                  <br>
                  <center><button type="button" class="btn btn-danger">ขออภัยอาหารหมดแล้วค่ะ</button></center>
                  
                </div>
              </div>
    </div>

    @endif

    @endforeach

    </div>
    
</div>
</body>
</html>