<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gift POS</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css">--}}
{{--    <script src="{{asset('js/app.js')}}" defer></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style>
        body {
            background: #FFFFFF;
            background-image: url('https://www.hackensackmeridianhealth.org/wp-content/uploads/2018/10/healthy-foods.jpg');
            background-attachment: fixed; 
            background-size: cover;  
            color: #000000;
            margin-top: 20px;
            font-family: 'Kanit', sans-serif;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }

        .bg {
          margin:0;
          background:#FFFFFF;
          padding: 20px;
          border-radius:20px;

        }

        .topic {
          color: #AAA;
          font-size: 40px;
          font-weight: 900;
          text-align: center;
        }

        .show_status {
          text-align: center;
          padding: 3px;
          color: #FFFFFF;
          border-radius:20px;

        }

        .list {
          padding: 20px;
        }

        .shadow:after{
          -moz-box-shadow: 5px 5px 5px #ccc;
            -webkit-box-shadow: 5px 5px 5px #ccc;
            box-shadow: 5px 5px 5px #ccc;

        }


    </style>
</head>
<body>
    <div class="container">
        <div class="card bg-light"> 
                <div class="row">
                        <div class="col-1">
                            <img src="https://image.flaticon.com/icons/svg/415/415587.svg"> 
                        </div>
                            <div class="text-dark"><br>
                                 <h1 style="font-size:2em; font-weight:bold; ">Welcome to Gift POS</h1></div>
                                    <div  class="col-4 offset-3"><br>
                                        <ul class="nav justify-content-end">
                                            <li class="nav-item">
                                        <a class="nav-link " href="{{route('table.index')}}">GUEST</a>
                                            </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle bg-dark text-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">STAFF</a>
                                            <div class="dropdown-menu">
                                              <a class="dropdown-item" href="{{route('admin.table.index')}}">Tables</a>
                                              <a class="dropdown-item" href="{{route('admin.food.index')}}">Foods</a>
                                              <a class="dropdown-item" href="/bill">Orders</a>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
        </div>
    </div><br>

    <div class="row justify-content-center">
        <div class="col-md-7 offset-md-3 bg shadow">
            
                <div class="col topic" > ออร์เดอร์อาหาร</div>

  

                  <div style="margin-top:20px; margin-bottom: 20px;" id="accordion">
                    @php
                      $count = 1
                    @endphp
                      @foreach ($order as $value)
                      <div class="card">
                      <div class="list-group-item list-group-item-action" href="#collapse{{$count}}" data-toggle="collapse" >
                          
                          <div class="row">
                            <div class="col-9">
                              <a class="collapsed card-link">
                                หมายเลขออเดอร์ {{$value->id}} , {{$value->table_stores->number}}
                              </a>
                            </div>
                          

                          <div class="col-2">

                            @if($value->status==1)
                              <div style="background: #6495ED;" class="show_status" role="alert">
                                  Order <span class="badge badge-light">New</span>
                              </div>

                            @elseif($value->status==2)
                              <div style="background: #FFD700;" class="show_status" role="alert">
                                  กำลังทำ...
                              </div>

                            @elseif($value->status==3)
                              <div style="background:  #00CC00;" class="show_status" role="alert">
                                  รอเรียกเก็บเงิน
                              </div>
                            @endif
                          </div>
                          <div class="col-1">
                              
                            <button style="font-size:30px" type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          </div>

                        </div>
                        <div id="collapse{{$count}}" class="collapse" data-parent="#accordion">
                          <div class="card-body">
                            <div class="row list">
                              <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      
                                      <th scope="col" style="width: 50%;">รายการอาหาร</th>
                                      <th scope="col">จำนวน</th>  
                                      <th scope="col">ราคา / ต่อจาน</th>
                                      <th scope="col">ราคารวม</th>
                                    </tr>
                                  </thead>
                                  @php
                                    $total = 0
                                  @endphp
                                  @foreach ($value->order_details as $food)
                                  <tbody>
                                    
                                    <tr>
                                      <td>{{$food->food_name}}</td>
                                      <td>{{$food->amount}}</td>
                                      <td>{{$food->price}}</td>
                                      <td>{{$food->total}}</td>
                                    </tr>
                                   
                                  </tbody>
                                  @php
                                    $total += $food->total
                                  @endphp
                                  @endforeach
                                  <tfoot>
                                    <tr>
                                      <td colspan="3">รวม</td>
                                      <td>{{$total}}</td>
                                    </tr>


                                    @if($value->status == 1 )

                                    <tr>
                                      <td colspan="3"></td>
                                      <td><a href="/bill/{{$value->id}}"><button type="button" class="btn btn-success">รับ Order</button></a></td>
                                    </tr>

                                    @elseif($value->status == 2 )

                                    <tr>
                                      <td colspan="3"></td>
                                      <td><a href="/bill/{{$value->id}}"><button type="button" class="btn btn-danger">ทำอาหารเสร็จแล้ว</button></a></td>
                                    </tr>

                                    @elseif($value->status == 3 )

                                    <tr>
                                      <td colspan="3"></td>
                                      <td><a href="/bill/{{$value->id}}"><button type="button" class="btn btn-info">เก็บเงิน</button></a></td>
                                    </tr>


                                    @endif

                                  </tfoot>
                                </table>
                          </div>
                          </div>
                        </div>
                      </div>
                      @php
                        $count += 1
                      @endphp
                      @endforeach
                  </div>

            
            


        </div>
    </div>





  </div>
</body>
</html>