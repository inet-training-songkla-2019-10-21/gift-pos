<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gift POS</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}">--}}
{{--    <script src="{{asset('js/app.js')}}"></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <style>
        body {
            background-image: url('https://www.hackensackmeridianhealth.org/wp-content/uploads/2018/10/healthy-foods.jpg');
            background-attachment: fixed; 
            margin-top: 20px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }

    </style>
</head>
<body>
<br><br>
<div class="container">
        <div class="card bg-light"> 
                <div class="row">
                        <div class="col-1">
                            <img src="https://image.flaticon.com/icons/svg/415/415587.svg"> 
                        </div>
                            <div class="text-dark"><br>
                                 <h1 style="font-size:2em; font-weight:bold; ">Welcome to Gift POS</h1></div>
                                    <div  class="col-4 offset-3"><br>
                                        <ul class="nav justify-content-end">
                                            <li class="nav-item">
                                        <a class="nav-link " href="{{route('table.index')}}">GUEST</a>
                                            </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle bg-dark text-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">STAFF</a>
                                            <div class="dropdown-menu">
                                              <a class="dropdown-item" href="{{route('admin.table.index')}}">Tables</a>
                                              <a class="dropdown-item" href="{{route('admin.food.index')}}">Foods</a>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
        </div>
    </div><br><br/>

<div class="container">
    <div class="row">
        <div class="col-3">
            <h1><a href="{{route('food.index')}}" class="main" style="color:black;"><strong>Edit Food</strong></a></h1>
        </div>
    </div>
    <br>
    <div class="row justify-content-center">
        <div class="col-6" align="left" style="margin: 30px">
            <h2 style="color: black"><u>แบบฟอร์มแก้ไขรายการอาหาร</u></h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form action="{{route("admin.food.edit")}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="bookname">แก้ไขรายการอาหาร</label>
                    <input
                        type="text" class="form-control"
                        name="name" id="foodname"
                        aria-describedby="foodHelp"
                        placeholder="ชื่อ อาหาร"
                        required
                        value={{$food->name}}
                    >
                    <small id="foodHelp" class="form-text text-muted">ชื่ออาหารที่คุณต้องการบันทึก</small>
                </div>
                <div class="form-group">
                    <label for="price">ราคา</label>
                    <input type="text" class="form-control" name="price" id="price" placeholder="ราคา" value={{$food->price}}>
                </div>
                <div class="form-group">
                    <select multiple class="form-control" id="status" name="status">
                        @if ($food->status == 1)
                            <option value=1 selected="selected">มี</option>
                            <option value=2>หมดแล้ว</option>
                        @elseif ($food->status == 2)
                            <option value=1>มี</option>
                            <option value=2 selected="selected">หมดแล้ว</option>
                        @endif
                    </select>
                     
                        
                    
                </div>
                <div class="form-group">
                    <input type="hidden" name="id" value="{{$food->id}}">
                </div>
                <br>
                <div align="right">
                    <button type="submit" class="btn btn-success">บันทึกการเปลี่ยนแปลง</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
