<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gift POS</title>
{{--    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css">--}}
{{--    <script src="{{asset('js/app.js')}}" defer></script>--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style>
        body {
            background: #FFFFFF;
            background-image: url('https://www.hackensackmeridianhealth.org/wp-content/uploads/2018/10/healthy-foods.jpg');
            color: #000000;
            margin-top: 20px;
        }
        .main {
            color: darkturquoise;
            text-decoration: none;
        }
        .main:hover {
            color: #98e1b7;
            text-decoration: none;
        }

        .grad {
            background: #FFFFFF; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* For Firefox 3.6 to 15 */
            background: linear-gradient(rgba(255, 255, 255, 0.3), #FFFFFF); /* Standard syntax */
            padding-bottom: 20px;
        }

    </style>
</head>
<body>
<br><br>
<div class="container">
    <div class="row grad">
        <div class="col-2">
            <img src="https://image.flaticon.com/icons/svg/415/415587.svg" width="100%"> 
        </div>
        <div class="col-5">
            <span style="font-size:4em; font-weight:bold; "> Gift POS</span>
            {{-- <h1><a href="{{route('book.index')}}" class="main"><strong>Book Store</strong></a></h1> --}}
        </div>
        <div class="col-2 offset-7" align="right">
            {{-- <h1><a href="{{route('book.create.page')}}" class="btn btn-primary">เพิ่มหนังสือ</a></h1> --}}
        </div>
    </div>

    <div style="  background: #FFFFFF; padding:20px" class="row">

    @foreach($foods as $key => $food)
        <div class="col">
            <div class="card" style="width: 18rem; margin: 20px;">
                    <img class="card-img-top" src="https://food.mthai.com/app/uploads/2018/07/Hot-and-sour-seafood-soup.jpg" alt="Card image cap">
                    <div class="card-body">
                      <div class="col-5"><h5 class="card-title">ต้มยำกุ้ง</h5></div>
                      <div class="col-5 offset-7" align="right"><input class="form-control" type="number" value="0" id="example-number-input"></div>
                      
                    </div>
                  </div>
        </div>


    @endforeach
 
    </div>
    

    {{-- <div class="row">
        <div class="col-12">
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>

                    <th scope="col">รายการอาหาร</th>
                    <th scope="col">ราคา</th>
                    <th scope="col">จำนวน</th>
                    
                </tr>
                </thead>
                <tbody>


                @foreach($books as $key => $book)
                    <tr>
                        <th scope="row">{{$key + 1}}</th>
                        <td>{{$book->name}}</td>
                        <td>{{$book->author}}</td>
                        <td>{{$book->describe}}</td>
                        <td>{{$book->price}}</td>
                        <td>{{$book->type}}</td>
                        <td><a href="{{route('book.edit.page', $book->id)}}" class="btn btn-outline-warning">แก้ไข</a></td>
                        <td><a href="{{route('book.delete', $book->id)}}" class="btn badge-danger">ลบ</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div> --}}
</div>
</body>
</html>