<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'TableStoreController@index')->name('table.index');
// Route::get('table/delete/{id}','TableStoreController@delete')->name('table.delete');
// Route::get('/table/create', 'TableStoreController@createPage')->name('table.create.page');
// Route::post('/table/create', 'TableStoreController@create')->name('table.create');
// Route::get('/table/edit/{id}','TableStoreController@editPage')->name('table.edit.page');
// Route::post('/table/edit','TableStoreController@edit')->name('table.edit');

Route::get('/onechat', 'OneChatController@getInfo');
Route::get('/onechat/message/{msg}', 'OneChatController@send');

Route::get('/foods', 'FoodController@info')->name('food.index');

Route::get('/order/{table_id}', 'OrderController@addTable')->name('order.addorder');
Route::get('/order/{table_id}/{order_id}', 'OrderController@addOrder');
Route::post('/order/{table_id}/{order_id}', 'OrderController@addFoodOrder');








// Route::get('/order/{table_id}', 'OrderController@addTable')->name('order.addorder');

















Route::get('/admin/login', 'FoodController@loginPage')->name('admin.page');
Route::post('/admin/login', 'FoodController@login')->name('admin.login');
Route::post('/admin/register', 'FoodController@register')->name('admin.register');

Route::group(['middleware' => ['food']], function () {
    Route::get('/admin', 'FoodController@admin')->name('admin');
    Route::get('/bill', 'BillController@billCheckPage');
    Route::get('/bill/{order_id}', 'BillController@billCheck');
    // Foods
    Route::get('/admin/foods', 'FoodController@infoAdmin')->name('admin.food.index');
    Route::get('/admin/foods/add', 'FoodController@createPage')->name('admin.food.create.page');
    Route::post('/admin/foods/add', 'FoodController@create')->name('admin.food.create');
    Route::get('/admin/foods/edit/{id}', 'FoodController@editPage')->name('admin.food.edit.page');
    Route::post('/admin/foods/edit', 'FoodController@edit')->name('admin.food.edit');
    Route::get('/admin/foods/delete/{id}', 'FoodController@delete')->name('admin.food.delete');


    Route::get('/admin/table', 'TableStoreController@indexAdmin')->name('admin.table.index');
    Route::get('/admin/table/add', 'TableStoreController@createPage')->name('admin.table.create.page');
    Route::post('/admin/table/add', 'TableStoreController@create')->name('admin.table.create');

    Route::get('/admin/table/edit/{id}', 'TableStoreController@editPage')->name('admin.table.edit.page');
    Route::post('/admin/table/edit', 'TableStoreController@edit')->name('admin.table.edit');
    Route::get('/admin/table/delete/{id}', 'TableStoreController@delete')->name('admin.table.delete');
});
