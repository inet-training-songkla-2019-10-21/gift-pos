<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use App\TableStore;
use App\User;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    $table = TableStore::all()->pluck('id')->toArray();
    $user = User::all()->pluck('id')->toArray();
    return [
        'table_id' => $faker->randomElement($table),
        'update_by' => $faker->randomElement($user),
    ];
});
