<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order_detail;
use App\Order;
use App\Food;
use Faker\Generator as Faker;

$factory->define(Order_detail::class, function (Faker $faker) {
    $order = Order::all()->pluck('id')->toArray();
    $food_id = Food::all()->pluck('id')->toArray();
    $food_name = Food::all()->pluck('name')->toArray();
    return [
        'order_id' => $faker->randomElement($order),
        'food_id' => $faker->randomElement($food_id),
        'food_name' => $faker->randomElement($food_name),
        'amount' => $faker->numberBetween(1, 3),
        'price' =>  '100',
        'total' =>  $faker->numberBetween(100, 300),

    ];
});
