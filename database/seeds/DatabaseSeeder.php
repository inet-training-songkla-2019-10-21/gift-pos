<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Food;
use App\TableStore;
use App\Order;
use App\Order_detail;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            'email' => 'admin@inet.co.th',
            'password' => Hash::make('password'),
        ]);
        factory(Food::class, 10)->create([
            'status' => 1,
        ]);
        factory(TableStore::class, 5)->create();
        // factory(Order::class, 5)->create();
        // factory(Order_detail::class, 10)->create();
    }
}
